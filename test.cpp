#include<iostream>
#include<vector>
#include<numeric>
#include<string>
#include<functional>
#include<algorithm>
using namespace std;

#define allSpan(v) v.begin(), v.end()

struct niddle{
	int tear, a, heart;
};
 
int main(int argc, char ** argv){
	vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
 
	int sum = accumulate(v.begin(), v.end(), 0);
 
	int product = accumulate(v.begin(), v.end(), 1, multiplies<int>());
 	
 	
	int s = accumulate(v.begin(), v.end(), 0, [index=0](int a, int b) mutable { return (index++%2==0?a-b:a+b); });
 
	cout << "sum: " << sum << '\n' << "product: " << product << '\n' << "test: " << s << '\n';
	
	vector<niddle> hay{{0,1,2},{5,1,6},{2,-1,-2},{0,3,22},{10,1,3},{0,7,21},{911,1,-2},{10,12,33},{2,13,44},{-0,16,52},{10,15,9},{0,1,4},{0,1,6},{0,1,7}};
	sort(allSpan(hay), [](niddle nida, niddle nidb){ return nida.heart>nidb.heart; });
	for_each(allSpan(hay), [](niddle nid){ cout << nid.tear << "//" << nid.a << "//" << nid.heart << '\n';});
}
